# Technical challenge

In order to run the application. Firstly run:

```
> npm install
```

In one terminal run:

```
> npm run server
```
Then in another terminal run:

```
> npm run start
```