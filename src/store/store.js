import { applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import allReducers from '../reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const history = createHistory();
const connectedHistory = routerMiddleware(history);

export default function configureStore(initialState) {
    return createStore(
        allReducers,
        initialState,
        composeEnhancers(applyMiddleware(
            thunk,
            connectedHistory,
        )),
    );
}
