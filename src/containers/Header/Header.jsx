import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import Clock from 'react-live-clock';
import * as rampActions from "../../actions/rampActions";
import * as mapActions from "../../actions/mapActions";
import { onScreenMaterial, onScreenSize, onScreenTotal } from "./helpers/utils"
import styles from './Header.scss';

class Header extends Component {
    render() {
        const data = this.props.rampState;
        return (
            <div className={styles.headerWrapper}>
                <div className={styles.header}>
                    <Container fluid>
                        <Row>
                            <Col md={11}>
                                <div className={styles.dataWrapper}>
                                    Total: {onScreenTotal(this.props.ramps)}/{data.total}
                                </div>
                                <div className={styles.dataWrapper}>
                                    <span/>
                                    Gravel: {onScreenMaterial(this.props.ramps, 'Gravel')}/{data.totalMaterials.gravel}<span/>
                                    Concrete: {onScreenMaterial(this.props.ramps, 'Concrete')}/{data.totalMaterials.concrete}<span/>
                                    Bitumen: {onScreenMaterial(this.props.ramps, 'Bitumen')}/{data.totalMaterials.bitumen}<span/>
                                    Interlock Conc Block: {onScreenMaterial(this.props.ramps, 'Interlock Conc Block')}/{data.totalMaterials.block}<span/>
                                    Earth: {onScreenMaterial(this.props.ramps, 'Earth')}/{data.totalMaterials.earth}<span/>
                                    Other: {onScreenMaterial(this.props.ramps, 'Other')}/{data.totalMaterials.other}<span/>
                                </div>
                                <div className={styles.dataWrapper}>
                                    <span/>
                                    0-50: {onScreenSize(this.props.ramps, [0,50])}/{data.totalSizes.small}<span/>
                                    50-200: {onScreenSize(this.props.ramps, [50,200])}/{data.totalSizes.medium}<span/>
                                    200-526: {onScreenSize(this.props.ramps, [200,526])}/{data.totalSizes.large}<span/>
                                </div>
                            </Col>
                            <Col md={1}>
                                <div className={styles.clockWrapper}>
                                    <div className={styles.clock}>
                                        <Clock ticking timzone="UTC" format="HH:mm:ss"/>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}

export const mapStateToProps = (state) => ({
    rampState: state.rampState,
    mapState: state.mapState,
});

export const mapDispatchToProps = (dispatch) => ({
    rampActions: bindActionCreators(rampActions, dispatch),
    mapActions: bindActionCreators(mapActions, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(Header);