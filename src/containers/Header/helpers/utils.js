export function onScreenMaterial(ramps, material){
    let total = 0;
    ramps.forEach((ramp) => {
        if(ramp.material === material){
            total++
        }
    });
    return total;
}

export function onScreenSize(ramps, size){
    let total = 0;
    ramps.forEach((ramp) => {
        if(size[0] <= ramp.size && ramp.size <= size[1]){
            total++;
        }
    });
    return total;
}

export function onScreenTotal(ramps){
    let total = 0;
    ramps.forEach(() => {
        total++;
    });
    return total;
}