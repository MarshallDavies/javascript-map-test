import React, { Component } from 'react';

import MapPolygon from '../../../../components/Map/Layers/Ramp/MapPolygon';
import MapTooltip from '../../../../components/Map/Layers/Ramp/MapTooltip';
import connect from "react-redux/es/connect/connect";

class MapRamp extends Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick = () => this.props.handlerFunctions.onClick(this.props.ramp);

    render() {
        const funcs = { onClick: this.onClick };
        const showInfoPopup = this.props.ramp.id === this.props.rampState.selectedRamp;

        return (
            <div>
                <MapPolygon geometry={this.props.ramp.geometry} handlerFunctions={funcs}/>
                {
                    showInfoPopup &&
                        <MapTooltip position={this.props.ramp.geometry[0]}>
                            <span>
                                Asset: <b>{this.props.ramp.assetNumber}</b>
                            <br/>
                                Type: <b>{this.props.ramp.type}</b>
                            <br/>
                                Material: <b>{this.props.ramp.material}</b>
                            <br/>
                                Size: <b>{this.props.ramp.size}</b>
                            <br/>
                                Status: <b>{this.props.ramp.status}</b>
                            <br/>
                                Updated: <b>{this.props.ramp.lastUpdated}</b>
                            </span>
                        </MapTooltip>
                }
            </div>
        );
    }
}

export const mapStateToProps = (state) => ({
    rampState: state.rampState,
});

export default connect(mapStateToProps)(MapRamp);