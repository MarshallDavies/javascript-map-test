import React, { Component } from 'react';
import {bindActionCreators} from "redux";
import * as rampActions from "../../actions/rampActions";
import * as mapActions from "../../actions/mapActions";
import connect from "react-redux/es/connect/connect";
import { viewportRamps} from "./helpers/util";
import Header from '../Header/Header'
import Map from './Map';

import styles from './MainMapScreen.scss';

class MainMapScreen extends Component {
    render() {
        const ramps = viewportRamps(Array.from(this.props.rampState.ramps.values()), this.props.mapState.bounds);
        return(
            <div className={styles.mapScreenWrapper}>
                <Header
                ramps={ramps}/>
                <div className={styles.mapContainer}>
                    <Map
                    ramps={ramps}/>
                </div>
            </div>
        );
    }
}

export const mapStateToProps = (state) => ({
    rampState: state.rampState,
    mapState: state.mapState,
});

export const mapDispatchToProps = (dispatch) => ({
    rampActions: bindActionCreators(rampActions, dispatch),
    mapActions: bindActionCreators(mapActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(MainMapScreen);