import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as rampActions from '../../actions/rampActions';
import * as mapActions from '../../actions/mapActions';
import GoogleMapWrapper from '../../components/Map/Google/GoogleMapWrapper'
import MapRampLayer from "../../components/Map/Layers/Ramp/MapRampLayer";

class Map extends Component {
    componentWillMount() {
        this.props.rampActions.retrieveRamps();
    }

    render() {
        const funcs = { updateBounds: this.props.mapActions.mapBoundsUpdated };
        return (
            <GoogleMapWrapper
                handlerFunctions={funcs}>
                {<MapRampLayer
                    ramps={this.props.ramps}
                    handlerFunctions={{
                        onClick: this.props.rampActions.selectRampToggle,
                    }}
                />}
            </GoogleMapWrapper>
        );
    }
}

export const mapStateToProps = (state) => ({
    rampState: state.rampState,
    mapState: state.mapState,
});

export const mapDispatchToProps = (dispatch) => ({
    rampActions: bindActionCreators(rampActions, dispatch),
    mapActions: bindActionCreators(mapActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Map);