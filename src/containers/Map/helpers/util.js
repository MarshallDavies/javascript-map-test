export function viewportRamps(values, rawBounds) {
    const ramps = [];
    if (rawBounds) {
        const mapBoundsNE = JSON.parse(JSON.stringify(rawBounds.getNorthEast()));
        const mapBoundsSW = JSON.parse(JSON.stringify(rawBounds.getSouthWest()));
        values.forEach((value) => {
            if (mapBoundsNE.lat >= value.geometry[0].lat &&
                value.geometry[0].lat >= mapBoundsSW.lat &&
                mapBoundsNE.lng >= value.geometry[0].lng &&
                value.geometry[0].lng >= mapBoundsSW.lng) {
                ramps.push(value);
            }
        });
    }
    return ramps;
}