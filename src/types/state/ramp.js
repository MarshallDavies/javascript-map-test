export type RampState = {
    busy,
    ramps,
    total,
    totalMaterials,
    totalSizes,
    selectedRamp,
    error,
};
