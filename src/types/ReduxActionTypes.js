import type { RampState } from './state/ramp';
import type { MapState } from './state/map'
import type { RampAction } from '../actions/rampActions';
import type { MapAction } from "../actions/mapActions";

export type Action =
    | RampAction
    | MapAction

export type State = {
    rampState: RampState,
    mapState: MapState
};

export type GetState = () => State;
export type PromiseAction = Promise<Action>;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type Dispatch = (action: Action | ThunkAction | PromiseAction | Array<Action>) => any;
