export default function parseRamp(rampList){
    return Object.assign({},{
        id: rampList.id,
        assetNumber: rampList.properties.asset_numb,
        type: rampList.properties.type,
        status: rampList.properties.status,
        material: rampList.properties.material,
        size: rampList.properties.area_,
        lastUpdated: rampList.properties.update_dat,
        geometry: getGeometry(rampList.geometry.coordinates[0][0]),
    });
}

function getGeometry(coordinates){
    const geometry = [];
    coordinates.forEach((item) => {
        const point = Object.assign({},{
           lat: item[1],
           lng: item[0],
        });
        geometry.push(point);
    });
    return geometry;
}