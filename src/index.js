import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from "react-router-redux";
import { withRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import configureStore from './store/store';
import createHistory from 'history/createBrowserHistory';
import App from './components/App/App';

const store = configureStore();
const history = createHistory();

const render = (Component) => {
    ReactDOM.render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Component />
            </ConnectedRouter>
        </Provider>,
        document.getElementById('root')
    );
};
render(withRouter(App));
