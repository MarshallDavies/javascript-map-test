import axios from 'axios';
import * as rampActions from '../actions/rampActions';

const GET_RAMPS_URL = 'http://localhost:8081/listAll';

export function getRamps() {
    return function action(dispatch) {
        dispatch(rampActions.retrieveRampsInProgress());

        const success = (result) => {
            dispatch(rampActions.retrieveRampsSuccessful(result));
        };
        const failure = (err) => {
            dispatch(rampActions.retrieveRampsFailed(err));
            throw (err);
        };
        return get(GET_RAMPS_URL, success, failure);
    };
}

function get(targetUrl, success, failure) {
    const request = axios.request(targetUrl);
    request.then(success).catch(failure);
}
