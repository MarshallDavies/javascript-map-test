import types from './actionTypes';

import * as rampApi from '../api/rampAPI';

export function retrieveRamps() {
    return rampApi.getRamps();
}

type loadStartedAction = {type: typeof types.RAMPS_LOAD_STARTED};
type loadSuccessAction = {type: typeof types.RAMPS_LOAD_SUCCESSFUL, payload: { rawRampList: any } };
type loadFailedAction = {type: typeof types.RAMPS_LOAD_FAILED, payload: { error: any } };
type selectRampAction = {type: typeof types.RAMP_SELECTED_TOGGLE, payload: { ramp: any } };

export type RampAction =
    | loadStartedAction
    | loadSuccessAction
    | loadFailedAction
    | selectRampAction

export function retrieveRampsInProgress(): loadStartedAction {
    return {
        type: types.RAMPS_LOAD_STARTED,
    };
}

export function retrieveRampsSuccessful(rawRampList): loadSuccessAction {
    return {
        type: types.RAMPS_LOAD_SUCCESSFUL,
        payload: {
            rawRampList,
        },
    };
}

export function retrieveRampsFailed(error): loadFailedAction {
    return {
        type: types.RAMPS_LOAD_FAILED,
        payload: {
            error,
        },
    };
}

export function selectRampToggle(ramp) {
    return {
        type: types.RAMP_SELECTED_TOGGLE,
        payload: {
            ramp,
        },
    };
}
