import types from './actionTypes';

type mapBoundsUpdateAction = {type: typeof types.MAP_BOUNDS_UPDATED, payload: { mapBounds: any } };

export type MapAction =
    | mapBoundsUpdateAction

export function mapBoundsUpdated(mapBounds): mapBoundsUpdateAction {
    return {
        type: types.MAP_BOUNDS_UPDATED,
        payload: {
            mapBounds,
        },
    };
}
