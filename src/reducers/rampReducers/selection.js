import parseRamp from '../../parsers/rampParser';

export function mapRamps(rawData) {
    const rampList = JSON.parse(JSON.stringify(rawData.features));
    const rampMap = new Map();
    rampList.map(parseRamp)
        .forEach((ramp) => {
            rampMap.set((ramp) => ramp.id, ramp);
        });
    return Array.from(rampMap.values());
}

export function rampTotal(rawData) {
    const ramps = JSON.parse(JSON.stringify(rawData.data));
    return ramps.totalFeatures;
}

export function toggleRampSelection(ramp, selectedRamp) {
    if (ramp !== selectedRamp){
        return ramp;
    }
}

export function materialTotal(rawData) {
    let gravel = 0;
    let concrete = 0;
    let bitumen = 0;
    let block = 0;
    let earth = 0;
    let other = 0;
    const rampList = JSON.parse(JSON.stringify(rawData.features));
    rampList.forEach((ramp) => {
        switch (ramp.properties.material) {
            case 'Gravel':
                gravel++;
                break;
            case 'Concrete':
                concrete++;
                break;
            case 'Bitumen':
                bitumen++;
                break;
            case 'Interlock Conc Block':
                block++;
                break;
            case 'Earth':
                earth++;
                break;
            case 'Other':
                other++;
                break;
            default:
                return;
        }
    });
    return Object.assign({},{
        'gravel': gravel,
        'concrete': concrete,
        'bitumen': bitumen,
        'block': block,
        'earth': earth,
        'other': other,
    });
}

export function sizeTotal(rawData) {
    let small = 0;
    let medium = 0;
    let large = 0;
    const rampList = JSON.parse(JSON.stringify(rawData.features));
    rampList.forEach((ramp) => {
        if(0 <= ramp.properties.area_ && ramp.properties.area_ <= 50){
            small++;
        }
        if(50 <= ramp.properties.area_ && ramp.properties.area_ <= 200){
            medium++;
        }
        if(200 <= ramp.properties.area_ && ramp.properties.area_ <= 526){
            large++;
        }
    });
    return Object.assign({},{
        'small': small,
        'medium': medium,
        'large': large,
    });
}