import types from '../../actions/actionTypes';

import {
    mapRamps,
    rampTotal,
    toggleRampSelection,
    materialTotal,
    sizeTotal,
    } from './selection';

export const initialState = {
    busy: false,
    total: null,
    ramps: new Map(),
    selectedRamp: null,
    totalMaterials: {
        'gravel': null,
        'concrete': null,
        'bitumen': null,
        'block': null,
        'earth': null,
        'other': null,
    },
    totalSizes: {
        'small': null,
        'medium': null,
        'large': null,
    },
    error: null,
};

export default function rampReducer(state = initialState, action) {
    switch (action.type) {
        case types.RAMPS_LOAD_STARTED:
            return {
                ...state,
                busy: true,
            };
        case types.RAMPS_LOAD_SUCCESSFUL:
            return {
                ...state,
                busy: false,
                total: rampTotal(action.payload.rawRampList),
                ramps: mapRamps(action.payload.rawRampList.data),
                totalMaterials : materialTotal(action.payload.rawRampList.data),
                totalSizes : sizeTotal(action.payload.rawRampList.data),
            };
        case types.RAMPS_LOAD_FAILED:
            return {
                ...state,
                busy: false,
                error: action.payload.error,
            };
        case types.RAMP_SELECTED_TOGGLE:
            return {
                ...state,
                selectedRamp: toggleRampSelection(action.payload.ramp.id, state.selectedRamp),
            };
        default:
            return state;
    }
}
