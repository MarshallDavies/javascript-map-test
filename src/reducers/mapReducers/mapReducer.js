import types from '../../actions/actionTypes';

export const initialState = {
    bounds: null
};

export default function mapReducer(state = initialState, action) {
    switch (action.type) {
        case types.MAP_BOUNDS_UPDATED:
            return {
                ...state,
                bounds: action.payload.mapBounds,
            };
        default:
            return state;
    }
}
