import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import rampReducer from './rampReducers/rampReducer';
import mapReducer from './mapReducers/mapReducer';

export default combineReducers({
    router: routerReducer,
    rampState: rampReducer,
    mapState: mapReducer,
});
