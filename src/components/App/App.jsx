import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import MainMapScreen from '../../containers/Map/MainMapScreen';

export default class App extends Component {
  render() {
    return (
      <div>
        <Switch>
            <Route exact path="/" component={MainMapScreen} />
            <Route render={() => <h1>Page not found</h1>} />
        </Switch>
      </div>
    );
  }
}
