import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';

const mapStyles = require('./mapStyles');

export class BaseMap extends Component {
    map: {
        getBounds: Function,
    };
    exposeMap: Function;

    constructor(props) {
        super(props);

        this.onMapIdle = this.onMapIdle.bind(this);
        this.exposeMap = this.exposeMap.bind(this);
    }

    componentDidMount() {
        this.props.handlerFunctions.updateBounds(this.map.getBounds());
    }

    onMapIdle() {
        this.props.handlerFunctions.updateBounds(this.map.getBounds());
    }

    exposeMap(mapEl) {
        this.map = mapEl;
    }

    render() {
        return (
            <GoogleMap
                ref={this.exposeMap}
                onIdle={this.onMapIdle}
                defaultZoom={10}
                defaultCenter={{ lat: -27.95528695216527, lng: 153.40945770032275 }}
                defaultOptions={{
                    mapTypeControl: false,
                    streetViewControl: false,
                    fullscreenControl: false,
                    styles: mapStyles,
                }}>
                { this.props.children }
            </GoogleMap>
        );
    }
}

const GoogleMapWrapper = withScriptjs(withGoogleMap((props) => {
    const { google } = window;
    return (
        <BaseMap
            {...props}
            addListenerFunction={google.maps.event.addListener}
        />
    );
}));

const getMapDiv = () => (<div style={{ height: '100%' }} />);

GoogleMapWrapper.defaultProps = {
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyAPBzCRDlZEQ1hi5YzpVM1LrXNsKkIRBG4",
    loadingElement: getMapDiv(),
    containerElement: getMapDiv(),
    mapElement: getMapDiv(),
};

export default GoogleMapWrapper;
