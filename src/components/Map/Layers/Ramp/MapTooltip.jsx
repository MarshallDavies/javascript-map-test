import React, { Component } from 'react';
import { InfoWindow } from 'react-google-maps';

export default class MapTooltip extends Component {
    render() {
        const positionObject = { lat: this.props.position.lat, lng: this.props.position.lng };
        return (
            <InfoWindow position={positionObject}>
                { this.props.children }
            </InfoWindow>
        );
    }
}
