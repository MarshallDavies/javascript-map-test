import React, { Component } from 'react';
import { Polygon } from 'react-google-maps';

export default class MapPolygon extends Component {
    render() {
        const { onClick } = this.props.handlerFunctions;

        const funcObject = { onClick };
        return (
            <Polygon
                {...funcObject}
                paths={this.props.geometry}
                options={{
                 fillColor: "red",
                 fillOpacity: 0.4,
                 strokeColor: "red",
                 strokeOpacity: 1,
                 strokeWeight: 3
             }}/>
        );
    }
}
