import React, { Component } from 'react';
import MapRamp from "../../../../containers/Map/Layers/Ramp/MapRamp";

export default class MapRampLayer extends Component {
    render() {
        return (
            <div>
                {
                    this.props.ramps.map((ramp) => (
                        <MapRamp
                            key={ramp.id}
                            ramp={ramp}
                            handlerFunctions={this.props.handlerFunctions}
                        />
                    ))
                }
            </div>
        );
    }
}
