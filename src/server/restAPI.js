const express = require('express');
const app = express();
const fs = require("fs");

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/listAll', function (req, res, err) {
    if (err) { return next(err); }
    fs.readFile( "../../data/boat_ramps.geojson", 'utf8', function (err, data) {
        console.log('Data retrieved');
        res.end(data);
    });
});

const server = app.listen(8081, () => {
    const host = server.address().address;
    const port = server.address().port;
    console.log(`Listening at http://${host}:${port}`)
});